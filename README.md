# README #


### What is this repository for? ###

* This repository is an adaptation from the PCL rgbd people detection example.
* http://pointclouds.org/documentation/tutorials/ground_based_rgbd_people_detection.php


### How do I get set up? ###

* How to run: Clone this repository in to your local machine.
* Configuration: PCL 1.8.0. Instruction can be found (https://www.youtube.com/watch?v=N3GCAD9bvwE)
* Dependencies: Boost, Eigen, Flann, VTK, Qhull, OpenNI2, Qt. 
