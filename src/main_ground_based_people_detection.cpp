#include <pcl/console/parse.h>
#include <pcl/point_types.h>
#include <pcl/common/time.h>
#include <pcl/visualization/pcl_visualizer.h>    
#include <pcl/io/openni2_grabber.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/people/ground_based_people_detection_app.h>
#include <kinect2_grabber.h>
#include <pcl/io/pcd_io.h>

typedef pcl::PointXYZRGBA PointT;
typedef pcl::PointCloud<PointT> PointCloudT;

// PCL viewer //
pcl::visualization::PCLVisualizer viewer("PCL Viewer");

// Mutex: //
boost::mutex cloud_mutex;

enum { COLS = 640, ROWS = 480 };

int print_help()
{
	cout << "*******************************************************" << std::endl;
	cout << "Ground based people detection app options:" << std::endl;
	cout << "   --help    <show_this_help>" << std::endl;
	cout << "   --svm     <path_to_svm_file>" << std::endl;
	cout << "   --conf    <minimum_HOG_confidence (default = -1.5)>" << std::endl;
	cout << "   --min_h   <minimum_person_height (default = 1.3)>" << std::endl;
	cout << "   --max_h   <maximum_person_height (default = 2.3)>" << std::endl;
	cout << "*******************************************************" << std::endl;
	return 0;
}

void cloud_cb_(const PointCloudT::ConstPtr &callback_cloud, PointCloudT::Ptr& cloud,
	bool* new_cloud_available_flag)
{
	cloud_mutex.lock();    // for not overwriting the point cloud from another thread
	*cloud = *callback_cloud;
	*new_cloud_available_flag = true;
	cloud_mutex.unlock();
}

/*
bool pcl::people::GroundBasedPeopleDetectionApp<PointT>::compute(std::vector<pcl::people::PersonCluster<PointT> >& clusters)
{
	// Check if all mandatory variables have been set:
	if (!ground_coeffs_set_)
	{
		PCL_ERROR("[pcl::people::GroundBasedPeopleDetectionApp::compute] Floor parameters have not been set or they are not valid!\n");
		return (false);
	}
	if (cloud_ == NULL)
	{
		PCL_ERROR("[pcl::people::GroundBasedPeopleDetectionApp::compute] Input cloud has not been set!\n");
		return (false);
	}
	if (!intrinsics_matrix_set_)
	{
		PCL_ERROR("[pcl::people::GroundBasedPeopleDetectionApp::compute] Camera intrinsic parameters have not been set!\n");
		return (false);
	}
	if (!person_classifier_set_flag_)
	{
		PCL_ERROR("[pcl::people::GroundBasedPeopleDetectionApp::compute] Person classifier has not been set!\n");
		return (false);
	}

	// Fill rgb image:
	rgb_image_->points.clear();                            // clear RGB pointcloud
	extractRGBFromPointCloud(cloud_, rgb_image_);          // fill RGB pointcloud

														   // Downsample of sampling_factor in every dimension:
	if (sampling_factor_ != 1)
	{
		PointCloudPtr cloud_downsampled(new PointCloud);
		cloud_downsampled->width = (cloud_->width) / sampling_factor_;
		cloud_downsampled->height = (cloud_->height) / sampling_factor_;
		cloud_downsampled->points.resize(cloud_downsampled->height*cloud_downsampled->width);
		cloud_downsampled->is_dense = cloud_->is_dense;
		for (uint32_t j = 0; j < cloud_downsampled->width; j++)
		{
			for (uint32_t i = 0; i < cloud_downsampled->height; i++)
			{
				(*cloud_downsampled)(j, i) = (*cloud_)(sampling_factor_*j, sampling_factor_*i);
			}
		}
		(*cloud_) = (*cloud_downsampled);
	}

	applyTransformationPointCloud();

	filter();

	// Ground removal and update:
	pcl::IndicesPtr inliers(new std::vector<int>);
	boost::shared_ptr<pcl::SampleConsensusModelPlane<PointT> > ground_model(new pcl::SampleConsensusModelPlane<PointT>(cloud_filtered_));
	ground_model->selectWithinDistance(ground_coeffs_transformed_, 2 * voxel_size_, *inliers);
	no_ground_cloud_ = PointCloudPtr(new PointCloud);
	pcl::ExtractIndices<PointT> extract;
	extract.setInputCloud(cloud_filtered_);
	extract.setIndices(inliers);
	extract.setNegative(true);
	extract.filter(*no_ground_cloud_);
	if (inliers->size() >= (300 * 0.06 / voxel_size_ / std::pow(static_cast<double> (sampling_factor_), 2)))
		ground_model->optimizeModelCoefficients(*inliers, ground_coeffs_transformed_, ground_coeffs_transformed_);
	else
		PCL_INFO("No groundplane update!\n");

	// Euclidean Clustering:
	std::vector<pcl::PointIndices> cluster_indices;
	typename pcl::search::KdTree<PointT>::Ptr tree(new pcl::search::KdTree<PointT>);
	tree->setInputCloud(no_ground_cloud_);
	pcl::EuclideanClusterExtraction<PointT> ec;
	ec.setClusterTolerance(2 * voxel_size_);
	ec.setMinClusterSize(min_points_);
	ec.setMaxClusterSize(max_points_);
	ec.setSearchMethod(tree);
	ec.setInputCloud(no_ground_cloud_);
	ec.extract(cluster_indices);

	// Head based sub-clustering //
	pcl::people::HeadBasedSubclustering<PointT> subclustering;
	subclustering.setInputCloud(no_ground_cloud_);
	subclustering.setGround(ground_coeffs_transformed_);
	subclustering.setInitialClusters(cluster_indices);
	subclustering.setHeightLimits(min_height_, max_height_);
	subclustering.setMinimumDistanceBetweenHeads(heads_minimum_distance_);
	subclustering.setSensorPortraitOrientation(vertical_);
	subclustering.subcluster(clusters);

	// Person confidence evaluation with HOG+SVM:
	if (vertical_)  // Rotate the image if the camera is vertical
	{
		swapDimensions(rgb_image_);
	}
	for (typename std::vector<pcl::people::PersonCluster<PointT> >::iterator it = clusters.begin(); it != clusters.end(); ++it)
	{
		//Evaluate confidence for the current PersonCluster:
		Eigen::Vector3f centroid = intrinsics_matrix_transformed_ * (it->getTCenter());
		centroid /= centroid(2);
		Eigen::Vector3f top = intrinsics_matrix_transformed_ * (it->getTTop());
		top /= top(2);
		Eigen::Vector3f bottom = intrinsics_matrix_transformed_ * (it->getTBottom());
		bottom /= bottom(2);
		it->setPersonConfidence(person_classifier_.evaluate(rgb_image_, bottom, top, centroid, vertical_));
	}

	return (true);
}
*/

struct callback_args {
	// structure used to pass arguments to the callback function
	PointCloudT::Ptr clicked_points_3d;
	pcl::visualization::PCLVisualizer::Ptr viewerPtr;
};

struct callback_single {
	// structure used to pass arguments to the callback function
	PointCloudT::Ptr clicked_one;
	pcl::visualization::PCLVisualizer::Ptr viewerPtr;
};

void
pp_callback(const pcl::visualization::PointPickingEvent& event, void* args)
{
	struct callback_args* data = (struct callback_args *)args;
	if (event.getPointIndex() == -1)
		return;
	PointT current_point;
	event.getPoint(current_point.x, current_point.y, current_point.z);
	data->clicked_points_3d->points.push_back(current_point);
	// Draw clicked points in red:
	pcl::visualization::PointCloudColorHandlerCustom<PointT> red(data->clicked_points_3d, 255, 0, 0);
	data->viewerPtr->removePointCloud("clicked_points");
	data->viewerPtr->addPointCloud(data->clicked_points_3d, red, "clicked_points");
	data->viewerPtr->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 10, "clicked_points");
	std::cout << current_point.x << " " << current_point.y << " " << current_point.z << std::endl;
}

int main(int argc, char** argv)
{
	if (pcl::console::find_switch(argc, argv, "--help") || pcl::console::find_switch(argc, argv, "-h"))
		return print_help();

	// Algorithm parameters:
	std::string svm_filename = "C:/Users/Stewart/Desktop/PCLSetUp/pcl/people/data/trainedLinearSVMForPeopleDetectionWithHOG.yaml";
	float min_confidence = -1.5;
	float min_height = 1.3;
	float max_height = 2.3;
	float voxel_size = 0.06;
	Eigen::Matrix3f rgb_intrinsics_matrix;
	rgb_intrinsics_matrix << 525, 0.0, 319.5, 0.0, 525, 239.5, 0.0, 0.0, 1.0; // Kinect RGB camera intrinsics

	// Read if some parameters are passed from command line:
	pcl::console::parse_argument(argc, argv, "--svm", svm_filename);
	pcl::console::parse_argument(argc, argv, "--conf", min_confidence);
	pcl::console::parse_argument(argc, argv, "--min_h", min_height);
	pcl::console::parse_argument(argc, argv, "--max_h", max_height);

	// Read Kinect live stream:
	PointCloudT::Ptr cloud(new PointCloudT);
	bool new_cloud_available_flag = false;
	pcl::Grabber* grabber = new pcl::Kinect2Grabber();
	boost::function<void(const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr&)> f =
		boost::bind(&cloud_cb_, _1, cloud, &new_cloud_available_flag);
	grabber->registerCallback(f);
	grabber->start();

	// Wait for the first frame:
	while (!new_cloud_available_flag)
		boost::this_thread::sleep(boost::posix_time::milliseconds(10000));
	new_cloud_available_flag = false;

	cloud_mutex.lock();    // for not overwriting the point cloud

	// Display pointcloud:
	pcl::visualization::PointCloudColorHandlerRGBField<PointT> rgb(cloud);
	viewer.addPointCloud<PointT>(cloud, rgb, "input_cloud");
	viewer.setCameraPosition(0, 0, -2, 0, 1, 0, 0);

	// Add point picking callback to viewer:
	struct callback_args cb_args;
	PointCloudT::Ptr clicked_points_3d(new PointCloudT);
	cb_args.clicked_points_3d = clicked_points_3d;
	cb_args.viewerPtr = pcl::visualization::PCLVisualizer::Ptr(&viewer);
	viewer.registerPointPickingCallback(pp_callback, (void*)&cb_args);
	std::cout << "Shift+click on three floor points, then press 'Q'..." << std::endl;

	// Spin until 'Q' is pressed:
	viewer.spin();
	std::cout << "done." << std::endl;

	cloud_mutex.unlock();

	// Ground plane estimation:
	Eigen::VectorXf ground_coeffs;
	ground_coeffs.resize(4);
	std::vector<int> clicked_points_indices;
	for (unsigned int i = 0; i < clicked_points_3d->points.size(); i++)
		clicked_points_indices.push_back(i);
	pcl::SampleConsensusModelPlane<PointT> model_plane(clicked_points_3d);
	model_plane.computeModelCoefficients(clicked_points_indices, ground_coeffs);
	std::cout << "Ground plane: " << ground_coeffs(0) << " " << ground_coeffs(1) << " " << ground_coeffs(2) << " " << ground_coeffs(3) << std::endl;

	// Initialize new viewer:
	pcl::visualization::PCLVisualizer viewer("PCL Viewer");          // viewer initialization
	viewer.setCameraPosition(0, 0, -2, 0, 1, 0, 0);

	// Create classifier for people detection:  
	pcl::people::PersonClassifier<pcl::RGB> person_classifier;
	person_classifier.loadSVMFromFile(svm_filename);   // load trained SVM

	// People detection app initialization:
	pcl::people::GroundBasedPeopleDetectionApp<PointT> people_detector;    // people detection object
	people_detector.setVoxelSize(voxel_size);                        // set the voxel size
	people_detector.setIntrinsics(rgb_intrinsics_matrix);            // set RGB camera intrinsic parameters
	people_detector.setClassifier(person_classifier);                // set person classifier
	//people_detector.setHeightLimits(min_height, max_height);         // set person classifier
	//people_detector.setSensorPortraitOrientation(true);             // set sensor orientation to vertical

	// For timing:
	static unsigned count = 0;
	static double last = pcl::getTime();
	pcl::ModelCoefficients sphere_coeff;
	sphere_coeff.values.resize(4);
	sphere_coeff.values[3] = 0.2;
	std::string id = "";
	PointCloudT::Ptr clicked_one(new PointCloudT);
	// Main loop:
	while (!viewer.wasStopped())
	{
		if (new_cloud_available_flag && cloud_mutex.try_lock())    // if a new cloud is available
		{
			new_cloud_available_flag = false;

			// Perform people detection on the new cloud:
			std::vector<pcl::people::PersonCluster<PointT> > clusters;   // vector containing persons clusters
			people_detector.setInputCloud(cloud);
			people_detector.setGround(ground_coeffs);                    // set floor coefficients
			people_detector.compute(clusters);                           // perform people detection

			ground_coeffs = people_detector.getGround();                 // get updated floor coefficients

			// Draw cloud and people bounding boxes in the viewer:
			viewer.removeAllPointClouds();
			viewer.removeAllShapes();
			pcl::visualization::PointCloudColorHandlerRGBField<PointT> rgb(cloud);
			viewer.addPointCloud<PointT>(cloud, rgb, "input_cloud");
			if (clusters.size() > 0) {
				for (int i = 0; i < clusters.size(); ++i) {
					sphere_coeff.values[0] = clusters[i].getX();
					sphere_coeff.values[1] = clusters[i].getY();
					sphere_coeff.values[2] = clusters[i].getZ();
					id = "sphere_" + i;
					viewer.addSphere(sphere_coeff, id);
					viewer.spinOnce();

				}
			}
			unsigned int k = 0;
			for (std::vector<pcl::people::PersonCluster<PointT> >::iterator it = clusters.begin(); it != clusters.end(); ++it)
			{
				if (it->getPersonConfidence() > min_confidence)             // draw only people with confidence above a threshold
				{
					// draw theoretical person bounding box in the PCL viewer:
					it->drawTBoundingBox(viewer, k);
					k++;
				}
			}
			std::cout << k << " people found" << std::endl;
			viewer.spinOnce();

			// Display average framerate:
			if (++count == 30)
			{
				double now = pcl::getTime();
				std::cout << "Average framerate: " << double(count) / double(now - last) << " Hz" << std::endl;
				count = 0;
				last = now;
			}
			cloud_mutex.unlock();
		}
	}

	return 0;
}